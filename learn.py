
import os

import numpy as np
from keras.models import Sequential
from keras.layers import LSTM


# suppress warning
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

HIDDEN_SIZE = 128

model = Sequential()
model.add(LSTM(HIDDEN_SIZE, input_shape=(728, 1)))
model.compile(
    optimizer='rmsprop',
    loss='binary_crossentropy',
    metrics=['accuracy'])

data = np.array([
    [0.1, 0.2],
    [0.3, 0.4],
    [0.3, 0.2],
    [0.6, 0.2],
    [0.7, 0.9],
    [0.1, 0.4],
    [0.3, 0.2]
])
labels = np.array([
    0, 0, 0, 1, 1, 0, 1
])

model.fit(data, labels, batch_size=32)
classes = model.predict(np.array([0.15, 0.16]).reshape((1, 2)))
print(classes)
