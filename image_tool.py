
from io import BytesIO

from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QImage
from PyQt5.QtCore import QBuffer
from PyQt5.QtCore import QIODevice

from PIL import Image
from PIL import ImageDraw

import numpy as np


class WrappedMath(object):

    @staticmethod
    def normalize(array):
        b = np.sum(array)
        return np.array([(x / b) for x in array])

    @staticmethod
    def calc_luminosity(color):
        return 0.2126 * color[0] + 0.7152 * color[1] + 0.0722 * color[2]


class Conversion(object):

    @staticmethod
    def qpix_2_pil(qpixmap):
        return Conversion.qimage_2_pil(qpixmap.toImage())

    @staticmethod
    def qimage_2_pil(qimage):
        pio = BytesIO()
        qbuff = QBuffer()
        qbuff.open(QIODevice.ReadWrite)
        qimage.save(qbuff, "PNG")
        pio.write(qbuff.data())
        qbuff.close()
        pio.seek(0)
        return Image.open(pio)


class ImageFix(object):

    @staticmethod
    def fix(img, hist, intensity):
        # ensure pil
        if img is QImage:
            img = Conversion.qimage_2_pil(img)
        elif img is QPixmap:
            img = Conversion.qpix_2_pil(img)

        # ensure np array
        if hist is not np.ndarray:
            hist = np.asarray(hist)

        histlen = len(hist)

        def apply_histogram(lum):
            prev_h = hist[0]
            ratio = lum / 255.0
            for i, h in enumerate(hist):
                progress = float(i) / histlen
                if ratio < progress:
                    return 255.0 * ((prev_h * progress + h * ratio) / (progress + ratio))
                prev_h = h
            return 255.0

        new_img = Image.new(img.mode, img.size)

        px = img.load()
        new_px = new_img.load()
        for x in range(img.size[0]):
            for y in range(img.size[1]):
                color = px[x, y]
                lum = WrappedMath.calc_luminosity(color)
                histogram_applied_lum = apply_histogram(lum)

                rate = 1.0
                if lum != 0:
                    rate = histogram_applied_lum / lum

                new_color_f = [c * rate for c in color]
                new_px[x, y] = (
                    int(new_color_f[0]),
                    int(new_color_f[1]),
                    int(new_color_f[2]),
                    color[3]
                )

        return new_img


class ImageDebug(object):

    @staticmethod
    def add_text(img, text):
        draw = ImageDraw.Draw(img)
        tx_sz = draw.textsize(text)
        draw.text(
            (img.size[0] * 0.5 - tx_sz[0] * 0.5 + 1,
             img.size[1] * 0.5 - tx_sz[1] * 0.5 + 1),
            text,
            fill=(0, 0, 0)
        )
        draw.text(
            (img.size[0] * 0.5 - tx_sz[0] * 0.5 - 1,
             img.size[1] * 0.5 - tx_sz[1] * 0.5 - 1),
            text,
            fill=(0, 0, 0)
        )
        draw.text(
            (img.size[0] * 0.5 - tx_sz[0] * 0.5,
             img.size[1] * 0.5 - tx_sz[1] * 0.5),
            text,
            fill=(255, 255, 255)
        )
