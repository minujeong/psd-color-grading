
"""
Tool to help grading texture color

author: Minu Jeong
"""

import colorsys

import numpy as np

from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtCore import Qt

from psd_tools import PSDImage
from psd_tools.constants import BlendMode

from PIL import Image
from PIL.ImageQt import ImageQt

from win32com import client

from ui import mainwindow

from image_tool import Conversion
from image_tool import ImageFix
from image_tool import ImageDebug
from image_tool import WrappedMath


# Registry key for Photoshop CS5 Application classid
PHOTOSHOP_APPLICATION_REGKEY = "Photoshop.Application.12"


class ToolMain(mainwindow.Ui_MainWindow):

    # pyqt buffers
    #  - without reference to pixmap, it causes garbage collection,
    #    and as a result, causes crash.
    pyqt_buffer = []
    pyqt_old_buffer = []

    # histogram stages
    hist_stage_hue = None
    hist_stage_sat = None
    hist_stage_lum = None

    # preview stages
    layerpreview_stage = None
    correction_stage = None

    # element stages
    hue_stage = None
    sat_stage = None
    lum_stage = None

    _psd_path = None

    # detail view popup window
    detail_view = None

    @property
    def psd_path(self):
        if not self._psd_path:
            # photoshop not opened
            psapp = client.Dispatch(PHOTOSHOP_APPLICATION_REGKEY)
            if not psapp:
                print("Photoshop not installed")
                return ""

            # no active document
            doc = psapp.Application.ActiveDocument
            if not doc:
                return ""

            try:
                filepath = doc.Path + doc.Name
            except:
                # document never saved
                return ""
            self._psd_path = filepath.replace("\\", '/')
        return self._psd_path

    @property
    def correction_intensity(self):
        return \
            float(self.CorrectionIntensitySlider.value()) /\
            self.CorrectionIntensitySlider.maximum()

    @property
    def histogram_flag(self):
        item = self.HistogramFlags.currentItem()
        if not item:
            item = self.HistogramFlags.itemAt(0, 0)
            if item:
                self.HistogramFlags.setCurrentItem(item)
            else:
                return np.linspace(0.0, 1.0, num=128)

        hist_data, _ = np.histogram(np.asarray(
            Conversion.qpix_2_pil(
                item.icon(0).pixmap(154, 25)
            )))

        return WrappedMath.normalize(hist_data)

    def __init__(self, mainwin):
        self.setupUi(mainwin)

        def show_detail_popup(stage):
            if self.detail_view:
                self.detail_view.close()
                self.detail_view = None

            if not stage:
                return

            items = stage.items()
            view = None
            if items:
                view = QtWidgets.QGraphicsView()
                view.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
                view.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
                stage = QtWidgets.QGraphicsScene()
                pixmap = items[0].pixmap()
                qimg = pixmap.toImage()
                img = Conversion.qimage_2_pil(qimg)
                img = img.resize((img.size[0] * 2, img.size[1] * 2))
                self.blit(img, stage)
                view.setScene(stage)
            if not view:
                return

            self.detail_view = QtWidgets.QWidget(mainwin, Qt.Window)
            layout = QtWidgets.QVBoxLayout()
            layout.addWidget(view)
            self.detail_view.setLayout(layout)
            self.detail_view.setMinimumSize(50, 50)
            self.detail_view.setWindowTitle("Zoom View")
            self.detail_view.show()

            self.detail_view.mousePressEvent = \
                lambda x: show_detail_popup(None)

        # histograms
        self.hist_stage_hue = QtWidgets.QGraphicsScene()
        self.HistogramHueGraphicsView.setScene(self.hist_stage_hue)
        self.HistogramHueGraphicsView.mousePressEvent = \
            lambda x: show_detail_popup(self.hist_stage_hue)

        self.hist_stage_sat = QtWidgets.QGraphicsScene()
        self.HistogramSaturationGraphicsView.setScene(self.hist_stage_sat)
        self.HistogramSaturationGraphicsView.mousePressEvent = \
            lambda x: show_detail_popup(self.hist_stage_sat)

        self.hist_stage_lum = QtWidgets.QGraphicsScene()
        self.HistogramLumGraphicsView.setScene(self.hist_stage_lum)
        self.HistogramLumGraphicsView.mousePressEvent = \
            lambda x: show_detail_popup(self.hist_stage_lum)

        # previews - org/fix
        self.layerpreview_stage = QtWidgets.QGraphicsScene()
        self.LayerGraphicsView.setScene(self.layerpreview_stage)
        self.LayerGraphicsView.mousePressEvent = \
            lambda x: show_detail_popup(self.layerpreview_stage)

        self.correction_stage = QtWidgets.QGraphicsScene()
        self.CorrectionGraphicsView.setScene(self.correction_stage)
        self.CorrectionGraphicsView.mousePressEvent = \
            lambda x: show_detail_popup(self.correction_stage)

        # previews - hue/sat/lum
        self.hue_stage = QtWidgets.QGraphicsScene()
        self.HueGraphicsView.setScene(self.hue_stage)
        self.HueGraphicsView.mousePressEvent = \
            lambda x: show_detail_popup(self.hue_stage)

        self.sat_stage = QtWidgets.QGraphicsScene()
        self.SaturationGraphicsView.setScene(self.sat_stage)
        self.SaturationGraphicsView.mousePressEvent = \
            lambda x: show_detail_popup(self.sat_stage)

        self.lum_stage = QtWidgets.QGraphicsScene()
        self.LuminosityGraphicsView.setScene(self.lum_stage)
        self.LuminosityGraphicsView.mousePressEvent = \
            lambda x: show_detail_popup(self.lum_stage)

        # signal: ps -> tool button
        self.UpdateFromActiveDocButton.clicked.connect(self.update_active_doc)

        # signals: histogram flags
        self.AddToFlagButton.clicked.connect(self.add_to_flags)
        self.RemoveFromFlagButton.clicked.connect(self.remove_from_flags)
        self.ClearFlagsButton.clicked.connect(self.clear_flags)

        # signals: big trees
        self.PSDTree.itemSelectionChanged.connect(self.select_layer_event)
        self.HistogramFlags.itemSelectionChanged.connect(self.select_layer_event)

        # signal: silder
        self.CorrectionIntensitySlider.valueChanged.connect(self.select_layer_event)

        # signal: action - exit
        self.ExitButton.clicked.connect(lambda e: mainwin.close())

        # initlaize psd
        self.update_active_doc()

    def get_target_resize(self, org_size, target_size):
        """
        get target size with maintaining image ratio
        """

        ow = org_size[0]
        oh = org_size[1]
        rw = target_size[0] / ow
        rh = target_size[1] / oh
        r = min(rw, rh)
        return (int(ow * r), int(oh * r))

    def blit(self, img, stage):
        pixmap_image = ImageQt(img)
        pixmap = QtGui.QPixmap.fromImage(pixmap_image)
        pixmap_item = None
        items = stage.items()
        if items:
            pixmap_item = items[0]
            pixmap_item.setPixmap(pixmap)
        else:
            pixmap_item = stage.addPixmap(pixmap)

        # maintain reference to buffer to prevent crash
        self.pyqt_buffer.append(pixmap_image)
        self.pyqt_buffer.append(pixmap_item)
        self.pyqt_buffer.append(pixmap)

        return pixmap_item

    def update_layers_info(self, psdpath):
        if not psdpath:
            return

        def add_to_tree(layer, parent):
            if not self.PSDTree:
                return

            item = QtWidgets.QTreeWidgetItem()
            if hasattr(layer, "name"):
                item.setText(0, layer.name)
            item.setData(0, Qt.UserRole, layer)
            parent.addChild(item)
            return item

        leaf_layers = []

        def recurse(layer, parent):
            """
            layer: PSDImage Layer
            parent: QTreeWidgetItem
            """

            item = add_to_tree(layer, parent)
            if not hasattr(layer, "layers"):
                leaf_layers.append(layer)
                return

            for sublayer in layer.layers:
                recurse(sublayer, item)

        # root item
        psd_img = PSDImage.load(psdpath)
        root_item = QtWidgets.QTreeWidgetItem()
        root_item.setText(0, "document")
        root_item.setData(0, Qt.UserRole, None)
        self.PSDTree.addTopLevelItem(root_item)

        # initiate recursive call
        for layer in psd_img.layers:
            recurse(layer, root_item)

        # return for debugging
        return leaf_layers

    def update_active_doc(self, e=None):
        self._psd_path = None
        self.PSDTree.clear()
        self.update_layers_info(self.psd_path)
        self.PSDTree.expandAll()

    def add_to_flags(self, e=None):
        lum_items = self.hist_stage_lum.items()
        if lum_items:
            size = QtCore.QSize(154, 25)
            hist_pixmap = lum_items[0].pixmap().scaled(size)
            item = QtWidgets.QTreeWidgetItem()
            item.setSizeHint(0, size)
            icon = QtGui.QIcon(hist_pixmap)
            item.setIcon(0, icon)
            self.HistogramFlags.addTopLevelItem(item)

    def remove_from_flags(self, e=None):
        for item in self.HistogramFlags.selectedItems():
            self.HistogramFlags.takeTopLevelItem(
                self.HistogramFlags.indexOfTopLevelItem(
                    item))

    def clear_flags(self, e=None):
        self.HistogramFlags.clear()

    def select_layer_event(self):
        items = self.PSDTree.selectedItems()
        if not items:
            return

        item = items[0]
        layer = item.data(0, Qt.UserRole)
        if not layer:
            return

        if layer.blend_mode != BlendMode.NORMAL:
            return

        img = None
        try:
            img = layer.as_PIL()
        except:
            return

        # empty layer
        if not img:
            return

        tw = int(self.LayerGraphicsView.width()) - 4
        th = int(self.LayerGraphicsView.height()) - 4
        ts = self.get_target_resize(img.size, (tw, th))
        img = img.resize(ts, Image.BICUBIC)

        self.pyqt_old_buffer = self.pyqt_buffer[:]
        self.pyqt_buffer.clear()

        self.update_layer_view(img)
        self.update_previews(img)

        self.pyqt_old_buffer.clear()

    def generate_option_from_template(self, e):
        """
        calculate
        """

        items = self.HistogramFlags.selectedItems()
        if not items:
            return

        item = items[0]
        pxmap = item.icon(0).pixmap(64, 32)
        img = Conversion.qpix_2_pil(pxmap)
        px = img.load()
        l = 0
        for x in range(img.size[0]):
            for y in range(img.size[1]):
                l += WrappedMath.calc_luminosity(px[x, y])
        self.target_value = int(l / (img.size[0] * img.size[1]))

    def update_layer_view(self, img):
        """
        Blit layer preview
        """

        if not img:
            return

        tw = int(self.LayerGraphicsView.width()) - 4
        th = int(self.LayerGraphicsView.height()) - 4
        ts = self.get_target_resize(img.size, (tw, th))

        img = img.resize(ts, Image.BICUBIC)
        r_img = img.copy()
        ImageDebug.add_text(r_img, "Photoshop")
        self.pixmap_item = self.blit(r_img, self.layerpreview_stage)

        tx = tw * 0.5 - ts[0] * 0.5
        ty = th * 0.5 - ts[1] * 0.5
        self.pixmap_item.setPos(tx, ty)

        self.blit_pre_fixed_image(img)

    def update_previews(self, img):
        """
        render histograms and previews
        """

        # blit preview images: hue/sat/lum
        # this is different from hsl system.
        # it's hue, saturation, and luminosity instead of value.
        #
        # [HACK/TODO]
        # This method is overly hardcoded.
        # Don't try to work with this method until I do refactorings.
        # TODO: separate histogram and preview rendering codes

        ts_hue = (int(self.HueGraphicsView.width()) - 4,
                  int(self.HueGraphicsView.height()) - 4)
        ts_sat = (int(self.SaturationGraphicsView.width()) - 4,
                  int(self.SaturationGraphicsView.height()) - 4)
        ts_lum = (int(self.LuminosityGraphicsView.width()) - 4,
                  int(self.LuminosityGraphicsView.height()) - 4)

        hue_img = Image.new("HSV", img.size)
        sat_img = Image.new("HSV", img.size)
        lum_img = Image.new("HSV", img.size)
        px_src = img.load()
        px_hue = hue_img.load()
        px_sat = sat_img.load()
        px_lum = lum_img.load()
        for x in range(img.size[0]):
            for y in range(img.size[1]):
                pixel = px_src[x, y][:3]
                lum = WrappedMath.calc_luminosity(pixel)
                px_lum[x, y] = (0, 0, int(lum))

                try:
                    hsv = colorsys.rgb_to_hsv(*(pixel))
                except:
                    continue

                if hsv[0]:
                    px_hue[x, y] = (int(hsv[0] * 255), 255, 255)

                if hsv[1]:
                    sat = int(hsv[1] * 255)
                    px_sat[x, y] = (128, 0, sat)

        # set images to center
        pixmap_item_hue = self.blit(
            hue_img.convert("RGB").resize(
                self.get_target_resize(img.size, ts_hue), Image.BICUBIC),
            self.hue_stage)
        tx_hue = ts_hue[0] * 0.5 - hue_img.size[0] * 0.5
        ty_hue = ts_hue[1] * 0.5 - hue_img.size[1] * 0.5
        pixmap_item_hue.setPos(tx_hue, ty_hue)

        pixmap_item_sat = self.blit(
            sat_img.convert("RGB").resize(
                self.get_target_resize(img.size, ts_sat), Image.BICUBIC),
            self.sat_stage)
        tx_sat = ts_sat[0] * 0.5 - sat_img.size[0] * 0.5
        ty_sat = ts_sat[1] * 0.5 - sat_img.size[1] * 0.5
        pixmap_item_sat.setPos(tx_sat, ty_sat)

        pixmap_item_lum = self.blit(
            lum_img.convert("RGB").resize(
                self.get_target_resize(img.size, ts_lum), Image.BICUBIC),
            self.lum_stage)
        tx_lum = ts_lum[0] * 0.5 - lum_img.size[0] * 0.5
        ty_lum = ts_lum[1] * 0.5 - lum_img.size[1] * 0.5
        pixmap_item_lum.setPos(tx_lum, ty_lum)

        # histogram ignore pure black/white
        t_hist_hue = (int(self.HistogramHueGraphicsView.width()) - 4,
                      int(self.HistogramHueGraphicsView.height()) - 4)
        t_hist_sat = (int(self.HistogramSaturationGraphicsView.width()) - 4,
                      int(self.HistogramSaturationGraphicsView.height()) - 4)
        t_hist_lum = (int(self.HistogramLumGraphicsView.width()) - 4,
                      int(self.HistogramLumGraphicsView.height()) - 4)

        hist_img_hue = Image.new("HSV", t_hist_hue, (196, 196, 196))
        hist_img_sat = Image.new("HSV", t_hist_sat, (196, 196, 196))
        hist_img_lum = Image.new("HSV", t_hist_lum, (196, 196, 196))

        hue_arr = np.asarray(hue_img)[..., 0]
        sat_arr = np.asarray(sat_img)[..., 2]
        lum_arr = np.asarray(lum_img)[..., 2]

        hist_values_hue, hist_keys_hue = \
            np.histogram(hue_arr, min(hist_img_hue.size[0], 255), range=(1, 255))
        hist_values_sat, hist_keys_hue = \
            np.histogram(sat_arr, min(hist_img_sat.size[0], 255), range=(1, 255))
        hist_values_lum, hist_keys_hue = \
            np.histogram(lum_arr, min(hist_img_lum.size[0], 255), range=(1, 254))

        hist_value_hue_max = hist_values_hue.max()
        if hist_value_hue_max:
            hist_values_hue = hist_values_hue / hist_values_hue.max()
        hist_value_sat_max = hist_values_sat.max()
        if hist_value_sat_max:
            hist_values_sat = hist_values_sat / hist_values_sat.max()
        hist_value_lum_max = hist_values_lum.max()
        if hist_value_lum_max:
            hist_values_lum = hist_values_lum / hist_values_lum.max()

        px_hue = hist_img_hue.load()
        px_sat = hist_img_sat.load()
        px_lum = hist_img_lum.load()

        key_len = min(
            len(hist_values_hue),
            len(hist_values_sat),
            len(hist_values_lum)
        )

        unit_width = \
            min(hist_img_hue.size[0], hist_img_sat.size[0], hist_img_lum.size[0]) / \
            key_len

        for key in range(key_len):
            for y in range(hist_img_hue.size[1]):
                hue_value = hist_values_hue[key]
                sat_value = hist_values_sat[key]
                lum_value = hist_values_lum[key]

                if 0.95 - (y / hist_img_hue.size[1]) < hue_value:
                    for i in np.arange(unit_width):
                        x = int(key * unit_width + i)
                        px_hue[x, y] = (int(255 * (key / key_len)), 255, 255)

                if 0.95 - (y / hist_img_hue.size[1]) < sat_value:
                    for i in np.arange(unit_width):
                        x = int(key * unit_width + i)
                        px_sat[x, y] = (196, int(255 * (key / key_len)), 255)

                if 0.95 - (y / hist_img_hue.size[1]) < lum_value:
                    for i in np.arange(unit_width):
                        x = int(key * unit_width + i)
                        px_lum[x, y] = (0, 0, int(255 * (key / key_len)))

        self.blit(hist_img_hue.convert("RGB"), self.hist_stage_hue)
        self.blit(hist_img_sat.convert("RGB"), self.hist_stage_sat)
        self.blit(hist_img_lum.convert("RGB"), self.hist_stage_lum)

    def blit_pre_fixed_image(self, img):
        img = ImageFix.fix(
            img,
            self.histogram_flag,
            self.correction_intensity
        )

        ImageDebug.add_text(img, "Result")

        tw = int(self.CorrectionGraphicsView.width()) - 4
        th = int(self.CorrectionGraphicsView.height()) - 4

        self.pixmap_item = self.blit(img, self.correction_stage)

        tx = tw * 0.5 - img.size[0] * 0.5
        ty = th * 0.7 - img.size[1] * 0.5

        self.pixmap_item.setPos(tx, ty)


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    mainiwn = QtWidgets.QMainWindow()
    win = ToolMain(mainiwn)
    mainiwn.show()
    app.exec()
